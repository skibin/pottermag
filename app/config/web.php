<?php

use app\components\Cart;

$params = require(__DIR__ . '/params.php');
$basePath = dirname(__DIR__);
$webRoot = dirname($basePath);

$config = [
    'id' => 'basic',
    'basePath' => $basePath,
    'vendorPath'=>$webRoot . '/vendor',
    'bootstrap' => ['log'],
    'language'=>'ru',
    'aliases'=> [
        '@bower'=>'@vendor/bower-asset',
    ],
    'components' => [
//        'view' => [
//            'class' => '\ogheo\htmlcompress\View',
//            'compress' => YII_ENV_DEV ? false : true,
//            // ...
//        ],

        'cart'=>[
            'class'=>'app\components\Cart',
        ],
        'amocrm' => [
            'class' => 'yii\amocrm\Client',
            'subdomain' => 'pottermag', // Персональный поддомен на сайте amoCRM
            'login' => 'pottermag@yandex.ru', // Логин на сайте amoCRM

            'hash' => '11d3c042bbd77663cf439cac6127d8c2', // Хеш на сайте amoCRM

        
            // Для хранения ID полей можно воспользоваться хелпером
            'fields' => [
//                'StatusId' => 10525225,
//                'ResponsibleUserId' => 697344,
            ],
        ],
        'assetManager'=> [
            'basePath' => '@webroot/media/assets',
            'baseUrl' => '@web/media/assets',
//            'bundles' => require(__DIR__ . '/asset-prod.php')
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'unzDS-oximKg66FfwGbjvhmoutbkR1mo',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
            'rules' => require(__DIR__ . '/routes.php')
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', ],
    ];
}

return $config;
