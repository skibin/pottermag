<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\widgets\ActiveFormAsset;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot/media';
    public $baseUrl = '@web/media';
    public $css = [
//        'css/modal.css',
        'css/main.min.css',
        'css/slick.css',
        'css/site.css',
    ];
    public $js = [
        'js/bootstrap-notify.min.js',
        'js/slick.js',
        'js/scripts1.min.js',
//        'js/modal.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'yii\widgets\ActiveFormAsset',
        'yii\widgets\PjaxAsset',
        'yii\widgets\MaskedInputAsset',
        'yii\validators\ValidationAsset',
        'andkon\yii2kladr\assets\KladrAsset',
    ];
}
