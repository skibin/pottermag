<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m170715_214047_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'first_name' => $this->string()->null(),
            'last_name' => $this->string()->null(),
            'email' => $this->string()->null(),
            'phone' => $this->string()->null(),
            'address1' => $this->string()->null(),
            'address2' => $this->string()->null(),
            'address3' => $this->string()->null(),
            'address4' => $this->string()->null(),
            'address5' => $this->string()->null(),
            'address6' => $this->string()->null(),
            'delivery_id' => $this->integer()->null(),
            'delivery_amount' => $this->decimal(10,2)->defaultValue(0),
            'payment_type' => $this->integer()->defaultValue(1),
            'status' => $this->smallInteger(1)->defaultValue(0),
            'created_at'=>$this->integer()->notNull(),
            'updated_at'=>$this->integer()->notNull(),
            'coupon_id'=>$this->integer()->null(),
            'shiptor_id'=>$this->string()->null(),
            'point_id'=>$this->string()->null(),
            'amo_id'=>$this->string()->null(),


        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('orders');
    }
}
