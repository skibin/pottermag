<?php

use yii\db\Migration;

class m170717_081708_create_test_data extends Migration
{
    public function safeUp()
    {
        if (YII_ENV_DEV) {
            $this->insert("products", [
                'id' => 1,
                'title'=>"Гарри Поттер и <br>Философский камень",
                'description'=>"Гарри Поттер и <br>Философский камень",
                'seo_keyword'=>"Гарри Поттер и <br>Философский камень",
                'seo_desc'=>"Гарри Поттер и <br>Философский камень",
                'price'=>790.00,
                'created_at'=>time(),
                'updated_at'=>time(),
            ]);

            $this->insert("products", [
                'id' => 2,
                'title'=>"Гарри Поттер и <br>Тайная комната",
                'description'=>"Гарри Поттер и <br>Тайная комната",
                'seo_keyword'=>"Гарри Поттер и <br>Тайная комната",
                'seo_desc'=>"Гарри Поттер и <br>Тайная комната",
                'price'=>790.00,
                'created_at'=>time(),
                'updated_at'=>time(),
            ]);

            $this->insert("products", [
                'id' => 3,
                'title'=>"Гарри Поттер и <br>Узник Азкабана",
                'description'=>"Гарри Поттер и <br>Узник Азкабана",
                'seo_keyword'=>"Гарри Поттер и <br>Узник Азкабана",
                'seo_desc'=>"Гарри Поттер и <br>Узник Азкабана",
                'price'=>790.00,
                'created_at'=>time(),
                'updated_at'=>time(),
            ]);

            $this->insert("products", [
                'id' => 4,
                'title'=>"Гарри Поттер и <br>Кубок огня",
                'description'=>"Гарри Поттер и <br>Кубок огня",
                'seo_keyword'=>"Гарри Поттер и <br>Кубок огня",
                'seo_desc'=>"Гарри Поттер и <br>Кубок огня",
                'price'=>790.00,
                'created_at'=>time(),
                'updated_at'=>time(),
            ]);

            $this->insert("products", [
                'id' => 5,
                'title'=>"Гарри Поттер и <br>Орден Феникса",
                'description'=>"Гарри Поттер и <br>Орден Феникса",
                'seo_keyword'=>"Гарри Поттер и <br>Орден Феникса",
                'seo_desc'=>"Гарри Поттер и <br>Орден Феникса",
                'price'=>790.00,
                'created_at'=>time(),
                'updated_at'=>time(),
            ]);

            $this->insert("products", [
                'id' => 6,
                'title'=>"Гарри Поттер и <br>Принц-полукровка",
                'description'=>"Гарри Поттер и <br>Принц-полукровка",
                'seo_keyword'=>"Гарри Поттер и <br>Принц-полукровка",
                'seo_desc'=>"Гарри Поттер и <br>Принц-полукровка",
                'price'=>790.00,
                'created_at'=>time(),
                'updated_at'=>time(),
            ]);

            $this->insert("products", [
                'id' => 7,
                'title'=>"Гарри Поттер и <br>Дары Смерти",
                'description'=>"Гарри Поттер и <br>Дары Смерти",
                'seo_keyword'=>"Гарри Поттер и <br>Дары Смерти",
                'seo_desc'=>"Гарри Поттер и <br>Дары Смерти",
                'price'=>790.00,
                'created_at'=>time(),
                'updated_at'=>time(),
            ]);

            $this->insert("products", [
                'id' => 8,
                'title'=>"Каталог книг",
                'description'=>"Каталог книг",
                'seo_keyword'=>"Каталог книг",
                'seo_desc'=>"Гарри Поттер и <br>Дары Смерти",
                'price'=>4590.00,
                'created_at'=>time(),
                'updated_at'=>time(),
            ]);


        }

    }

    public function safeDown()
    {
        $this->truncateTable("products");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170717_081708_create_test_data cannot be reverted.\n";

        return false;
    }
    */
}
