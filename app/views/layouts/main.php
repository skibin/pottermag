<?php

/* @var $this \yii\web\View */
/* @var $content string */

use andkon\yii2kladr\Kladr;
use app\components\Cart;
use app\components\Shiptor;
use app\forms\CallbackForm;
use app\forms\QuestionForm;
use app\models\Order;
use app\models\OrderProduct;
use app\models\Product;
use dosamigos\selectize\SelectizeDropDownList;
use dosamigos\selectize\SelectizeTextInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\web\JsExpression;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\MaskedInput;
use yii\widgets\Pjax;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <meta name="description" content="">


    <link rel="shortcut icon" href="/media/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="/media/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/media/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/media/favicon/apple-touch-icon-114x114.png">
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">
    </script>

    <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#000">
    <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#000">
    <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#000">

</head>
<body>
<?php $this->beginBody() ?>
<?php
$session = Yii::$app->session;
/** @var Cart $cart */
$cart = Yii::$app->cart;
$order_id = $session->get($cart->getSessionKey());
$product_count = 0;
if ($order_id != null) {
    $product_count = OrderProduct::find()->andWhere(['order_id' => $order_id])->count();
}

?>
<div class="cart-button <?= ($product_count <= 0) ? "hidden" : '' ?>" data-toggle="modal" data-target="#cart-modal">
    <img class="cart" src="/media/img/busket.png" alt="">
    <img class="cart-hover" src="/media/img/buskethover.png" alt="">
    <img class="cart-active" src="/media/img/busketactive.png" alt="">
</div>
<header>
    <div class="e-wrapper">
        <div class="e-top-info">
            <div class="e-top-infobox e-infobox-logo">
                <a href="/">
                    <img src="/media/img/logo1.png" alt="" style="width: 150px;">
                </a>
            </div>
            <div class="e-top-infobox">
                <img src="/media/img/top-info1.png" alt="">
            </div>
            <div class="e-top-infobox">
                <img src="/media/img/top-info2.png" alt="">
                <p></p>
            </div>
            <div class="e-top-infobox">
                <img src="/media/img/top-info3.png" alt="">
            </div>
            <div class="e-top-infobox e-infobox-order">
                <a href="" class="phone">8 (499) 404-07-29</a>
                <a href="" class="order" data-toggle="modal" data-target="#callback-modal">Заказать звонок</a>
            </div>
        </div>
        <div class="top-logo">
            <img src="/media/img/top-title.png" alt="" class="pc-logo">
            <img src="/media/img/top-title-mob.png" alt="" class="mob-logo">
            <a href="#books" class="goto">Заказать сейчас</a>
        </div>
    </div>
</header>
<?= $content ?>
<footer>
    <div class="e-wrapper">
        <div class="e-title">
            <img src="/media/img/quations.png" alt="" class="hidden-mob">
            <img src="/media/img/quations-mob.png" alt="" class="visible-mob">
        </div>
        <p>Менеджер свяжется с вами через 10 минут</p>
        <a href="#" data-toggle="modal" data-target="#question-modal">задать вопрос</a>
        <p class="offerta" style="text-align: center; color: #fff; margin-top: 30px;margin-bottom: 0;width: 100%;">
            <a href="/agreement.pdf" style="text-align: center; color: #fff;">Публичная офферта</a> |
            <a href="/privacy.pdf" style="text-align: center; color: #fff;">Политика конфидициальности</a>
        </p>
    </div>

</footer>

<div class="container"></div>

<div class="modal fade" tabindex="-1" role="dialog" id="callback-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Заказать звонок</h4>
            </div>
            <div class="modal-body">

                <?php
                if (Yii::$app->session->hasFlash("success_callback")) {
                    $this->registerJs("$('#callback-modal').modal({show:true})")
                    ?>
                    <p>Мы вам обязательно перезвоним.</p>
                    <?php
                } else {
                    $form = ActiveForm::begin([
                        'id' => 'callback-form',
                        'enableClientValidation' => false
                    ]);
                    $callback_form = new CallbackForm();
                    ?>


                    <?= $form->field($callback_form, 'phone_number')->textInput(['autofocus' => true]) ?>


                    <div class="form-group">
                        <?= Html::submitButton('Заказать звонок', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                        <div class="clearfix"></div>
                    </div>


                    <?php
                    ActiveForm::end();
                } ?>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="question-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <?php if (Yii::$app->session->hasFlash("success_question")) {
                    ?>
                    <h4 class="modal-title"></h4>
                <?php
                } else {
                    ?>
                    <h4 class="modal-title">Задать вопрос</h4>
                <?php
                }

                ?>
            </div>
            <div class="modal-body">

                <?php
                if (Yii::$app->session->hasFlash("success_question")) {
                    $this->registerJs("$('#question-modal').modal({show:true});");
                    ?>
                    Спасибо за обращение мы Вам обязательно ответим.
                    <?php
                } else {

                    $form = ActiveForm::begin([
                        'id' => 'callback-form',
                        'enableClientValidation' => false
                    ]);
                    $callback_form = new QuestionForm();


                    ?>


                    <?= $form->field($callback_form, 'name')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($callback_form, 'email')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($callback_form, 'phone')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($callback_form, 'content')->textarea(['autofocus' => true]) ?>


                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                        <div class="clearfix"></div>
                    </div>


                    <?php
                    ActiveForm::end();
                }
                ?>


            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php
/** @var Product $item */
foreach (Product::find()->all() as $item) {
    ?>

    <div class="modal fade" tabindex="-1" role="dialog" id="oneclick-<?=$item->id?>-modal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Заполните форму</h4>
                </div>
                <div class="modal-body">

                    <?php

                    $form = ActiveForm::begin([
                        'id' => 'callback-form-' . $item->id,
                        'enableClientValidation' => false
                    ]);
                    $fast_order = new QuestionForm();
                    $fast_order->fast_order = true;
                    $fast_order->content = "Заказ в 1 клик: " . strip_tags($item->title);
                    ?>


                    <?= $form->field($fast_order, 'name')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($fast_order, 'email')->textInput(['autofocus' => true]) ?>
                    <?= $form->field($fast_order, 'phone')->textInput(['autofocus' => true]) ?>
                    <?= Html::activeHiddenInput($fast_order, 'fast_order')?>

                    <div class="form-group">
                        <?= Html::submitButton('Отправить', ['class' => 'pull-right btn-potter btn', 'name' => 'contact-button']) ?>
                        <div class="clearfix"></div>
                    </div>


                    <?php
                    ActiveForm::end();
                    ?>
                </div>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


<?php

    $this->registerJs("
    $(document).on('show.bs.modal', '#oneclick-" . $item->id . "-modal', function () {  roistat.event.send('oneclick_" . $item->id . "_r'); yaCounter45332526.reachGoal('oneclick_" . $item->id . "_r' ); ga('send', 'event', 'order', 'oneclick_"  . $item->id . "_r');});
    $(document).on('submit', '#callback-form-" . $item->id . "', function () { roistat.event.send('oneclick_" . $item->id . "'); yaCounter45332526.reachGoal('oneclick_" . $item->id . "' ); ga('send', 'event', 'order', 'oneclick_"  . $item->id . "');  });
    ");
}
?>


<?php
if (Yii::$app->session->hasFlash("success_order")) {
    $this->registerJs("$('#success-order-modal').modal({show:true});");
}
?>
<div class="modal fade" tabindex="-1" role="dialog" id="success-order-modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Спасибо за ваш заказ</h4>
            </div>
            <div class="modal-body">
                Менеджер скоро свяжется с Вами и уточнит все детали.

            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php

?>

<div class="modal fade" tabindex="-1" role="dialog" id="cart-modal">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><img src="/media/img/close.png" alt=""></button>
                <h4 class="modal-title">Корзина</h4>
            </div>
            <div class="modal-body">

                <?php
                $order = Order::findOne(Yii::$app->session->get(Yii::$app->cart->getSessionKey()));

                if ($order == null) {
                    $order = new Order();
                }

                $order->first_name = Yii::$app->session->get("first_name");
                $order->last_name = Yii::$app->session->get("last_name");
                $order->email = Yii::$app->session->get("email");
                $order->phone = Yii::$app->session->get("phone");


                $order->setScenario("update");

                $form = ActiveForm::begin([
                    'id' => 'order-form',
                ]);

                ?>
                <div class="e-basket-wrapper">
                    <div class="e-left-wrapper">
                        <div class="e-title"><img src="/media/img/basket.png" alt=""></div>
                        <?php
                        Pjax::begin(['id' => 'cart-table', 'timeout' => 7000]);

                        $products = OrderProduct::find()->andWhere(['order_id' => $order->id])->all();
                        $total_price = 0;
                        foreach ($products as $product) {
                            $p = Product::findOne($product->product_id);
                            $total_price += $product->price;
                        ?>

                        <div class="e-item-wrapper">
                            <div class="e-img-wrapper">
                                <img src="/media/img/books/<?=$p->id?>.png" alt="">
                                <div class="input-group number-spinner">
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" data-dir="dwn">
                                            <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                    </span>
                                    <input data-product-id="<?= $p->id ?>" min="1" type="text"
                                           class="qty form-control" value="<?= $product->count ?>"/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" data-dir="up">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <div class="e-info-wrapper">
                                <h5>Товар:</h5>
                                <h4><?= $p->title ?></h4>
                                <h5>Цена:</h5>
                                <h4><?= $p->price ?> руб</h4>
                                <a href="#" class="delete_from_cart"
                                   data-product-id="<?= $p->id ?>">удалить</a>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="final-price">
                            <h5>Итог: <span id="total-not-deliver"><?=$total_price?> руб.</span> </h5>
                            <h5 id="delivery-price-label"  class="">Итог c доставкой: <span id="total-summary">0  руб.</span></h5>
                        </div>
                        <script>
                            var total = <?=$total_price?>;
                        </script>
                        <?php Pjax::end();?>
                    </div>
                    <div class="e-right-wrapper">

                        <div id="accordion" class="" role="tablist" aria-multiselectable="true">
                            <div class="panel">
                                <div  class="panel-heading" role="tab">
                                    <div class="e-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#etab1" aria-expanded="true" aria-controls="etab1"><img src="/media/img/data.png" alt=""></div>
                                </div>
                                <div id="etab1" class="panel-collapse collapse in" role="tabpanel">
                                    <?= $form->field($order, 'first_name')->textInput(['autofocus' => true]) ?>
                                    <?= $form->field($order, 'last_name')->textInput(['autofocus' => true]) ?>
                                    <?= $form->field($order, 'email')->textInput(['autofocus' => true]) ?>
                                    <?= $form->field($order, 'phone')->widget(MaskedInput::className(), [
                                        'mask' => '+79999999999'
                                    ]) ?>
                                </div>
                            </div>
                            <div class="panel">
                                <div  class="panel-heading" role="tab">
                                    <div class="e-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#etab2" aria-expanded="false" aria-controls="etab2"><img src="/media/img/adres.png" alt=""></div>
                                </div>
                                <div id="etab2" class="panel-collapse collapse" role="tabpanel">
                                   
                                    <?= $form->field($order, 'address1')->widget(SelectizeDropDownList::className(), [

                                        'clientOptions' => [
                                            'options'=>[],
                                            'searchField'=>'title',
                                            'labelField'=>'title',
                                            'valueField'=>'id',
                                            'persist'=>true,
                                            'loadThrottle'=>600,
                                            'create'=>false,
                                            'load'=>new JsExpression("function(query, callback) {
                                            if (!query.length) return callback();
                                                $.ajax({
                                                    url: '/index.php/site/find-place',
                                                    type: 'GET',
                                                    dataType: 'json',
                                                    data: {
                                                        query: query,
                                                        
                                                    },
                                                    error: function() {
                                                        callback();
                                                    },
                                                    success: function(res) {
                                                        
                                                        callback(res);
                                                    }
                                                });
                                            
                                            }")
                                        ],
                                    ])
                                    ?>


                                    <?= $form->field($order, 'address2')->widget(SelectizeDropDownList::className(), [

                                        'clientOptions' => [
                                            'options'=>[],
                                            'searchField'=>'full_name',
                                            'labelField'=>'full_name',
                                            'valueField'=>'id',
                                            'persist'=>true,
                                            'loadThrottle'=>600,
                                            'create'=>true,
                                            'render'=> new JsExpression("
                                                    {
                                                    option_create: function(data, escape) {
                                                          return '<div class=\"create\">Добавить <strong>' + escape(data.input) + '</strong>&hellip;</div>';
                                                    }
                                                    }
                                            "),
                                            'load'=>new JsExpression("function(query, callback) {
                                            if (!query.length) return callback();
                                                $.ajax({
                                                    url: '/index.php/site/kladr-find?type=street&name=' + query + '&parent_id=' + window.country,
                                                    type: 'GET',
                                                    dataType: 'json',
                                                    data: {
                                                        query: query,
                                                        
                                                    },
                                                    error: function() {
                                                        callback();
                                                    },
                                                    success: function(res) {
                                                        callback(res);
                                                    }
                                                });
                                            
                                            }")
                                        ],
                                    ])
                                    ?>

                                    <?= $form->field($order, 'address3')->widget(SelectizeDropDownList::className(), [

                                        'clientOptions' => [
                                            'options'=>[],
                                            'searchField'=>'full_name',
                                            'labelField'=>'full_name',
                                            'valueField'=>'id',
                                            'persist'=>true,
                                            'loadThrottle'=>600,
                                            'create'=>true,
                                            'render'=> new JsExpression("
                                                    {
                                                    option_create: function(data, escape) {
                                                          return '<div class=\"create\">Добавить <strong>' + escape(data.input) + '</strong>&hellip;</div>';
                                                    }
                                                    }
                                            "),
                                            'load'=>new JsExpression("function(query, callback) {
                                            if (!query.length) return callback();
                                                $.ajax({
                                                    url: '/index.php/site/kladr-find?type=building&name=' + query + '&parent_id=' + window.street,
                                                    type: 'GET',
                                                    dataType: 'json',
                                                    data: {
                                                        query: query,
                                                        
                                                    },
                                                    error: function() {
                                                        callback();
                                                    },
                                                    success: function(res) {
                                                        callback(res);
                                                    }
                                                });
                                            
                                            }")
                                        ],
                                    ])
                                    ?>


                                    <?= $form->field($order, 'address4')->textInput(['autofocus' => true]) ?>
                                    <?= $form->field($order, 'address5')->hiddenInput()->label(false) ?>

                                    <div class="well">
                                        <span id="order-address5_2"></span>
                                    </div>
                                    <hr>


                                    <?php
                                    Pjax::begin(['id' => 'shiptor', 'timeout' => 7000]);
                                    $shiptor = new Shiptor();
                                    $delivery = [];
                                    $delivery_prices = [];
                                    if (Yii::$app->session->has("kladr")) {


                                        $result = $shiptor->calculateShipping(Yii::$app->session->get("kladr"), $order->id);
                                        if (isset($result['result']['methods'])) {
                                            ?>
                                                <div id="order-delivery_id">
                                                    <?php


                                                    foreach ($result['result']['methods'] as $method) {
                                                        if ($method['method']['category'] == 'delivery-point' ) {
                                                            continue;
                                                        }

                                                        $delivery_prices[999] = 0;
                                                        if (Yii::$app->session->get("kladr") == 77000000000) {
                                                            $delivery[999] = "Самовывоз со склада(0 ₽) ";
                                                        }

                                                        $delivery_prices[$method['method']['id']] = $method['cost']['total']['sum'];
                                                        $delivery[$method['method']['id']] = $method['method']['name'] . '(' . $method['cost']['total']['sum'] . '₽) - ' . $method['days'];


                                                        $couriers[$method['method']['id']] = [
                                                            'name'=>$method['method']['name'] . '(' . $method['cost']['total']['sum'] . '₽) - ' . $method['days'],
                                                        ];

                                                        ?>




                                                        <?php
                                                    }
                                                    echo $form->field($order, 'delivery_id')->radioList($delivery, [
                                                        'itemOptions'=>['class'=>'delivery']
                                                    ])
                                                    ?>




                                            </div>
                                            <?php

                                        }
                                    }
                                    echo "<script>";
                                    echo "var delivery_price = " . json_encode($delivery_prices) . ';';

                                    echo "</script>";


                                    echo $form->field($order, 'delivery_amount')->hiddenInput()->label(false);

                                    Pjax::end();

                                    ?>

                                </div>
                            </div>
                            <div class="panel">
                                <div  class="panel-heading" role="tab">
                                    <div class="e-title" role="button" data-toggle="collapse" data-parent="#accordion" href="#etab3" aria-expanded="false" aria-controls="etab3"><img src="/media/img/payment.png" alt=""></div>
                                </div>
                                <div id="etab3" class="panel-collapse collapse" role="tabpanel">
                                    <div class="form-group field-order-payment_type has-success">
                                        <label class="control-label">Тип оплаты</label>
                                        <input type="hidden" name="Order[payment_type]" value="">
                                        <div id="order-payment_type" aria-invalid="false">
                                            <div class="radio"><input type="radio" id="online" name="Order[payment_type]" value="3"> <label for="online">Оплата онлайн</label></div>
                                            <div class="radio"><input type="radio" id="cash" name="Order[payment_type]" value="2"> <label for="cash">Наложенный платеж</label></div></div>

                                    </div>
                                    <?php
                                    echo $form->errorSummary($order);
                                    ?>
                                    <?=Html::submitButton('Оформить заказ', ['class'=>'btn'])?>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>



                <div class="modal fade"  tabindex="-1" role="dialog" id="delivery-modal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                                </button>
                                <h4 class="modal-title">Выберите пункт</h4>
                            </div>
                            <div class="modal-body">
                                <?php Pjax::begin(['id'=>'map']);
                                if (Yii::$app->session->has("delivery_kladr")) {
                                    $shiptor = new Shiptor();
                                    echo $form->field($order, 'point_id')->hiddenInput()->label(false);

                                    $points_raw = $shiptor->getDeliveryPoint($session->get("delivery_kladr"), $session->get('delivery_courier'), $session->get('delivery_id'));

                                    $session->remove("delivery_kladr");
                                    $session->remove("delivery_courier");
                                    $session->remove("delivery_id");


                                    $points_res = [];
                                    foreach ($points_raw['result'] as $p) {
                                        $points_res[] = [
                                            'id'=>$p['id'],
                                            'courier'=>$p['courier'],
                                            'address'=>$p['address'],
                                            'phones'=>$p['phones'],
                                            'work_schedule'=>$p['work_schedule'],
                                            'trip_description'=>preg_replace('/[\r\n\t]/', '<br>', $p['trip_description']),
                                            'cod'=>$p['cod'],
                                            'kladr_id'=>$p['kladr_id'],
                                            'shipping_methods'=>$p['shipping_methods'],
                                            'lat'=>$p['gps_location']['latitude'],
                                            'lgt'=>$p['gps_location']['longitude'],

                                        ];
                                    }
                                    ?>
                                    <script>
                                        ymaps.ready(init);
                                        var myMap;

                                        function init() {
                                            myMap = new ymaps.Map("map-area", {
                                                <?php
                                                if (count($points_res)>=1) {
                                                ?>
                                                center: [<?=$points_res[0]['lat']?>, <?=$points_res[0]['lgt']?>],
                                                <?php } else { ?>
                                                center: [55.76, 37.64],
                                                <?php } ?>
                                                zoom: 10
                                            });
                                            <?php
                                            foreach ($points_res as $p) {
                                            ?>


                                            var placemark_<?=$p['id']?> = new ymaps.Placemark([<?=$p['lat']?>, <?=$p['lgt']?>], {
                                                balloonContent: "<a href='#' class='btn btn-success point' data-kladr='<?=$p['kladr_id']?>' data-id='<?=$p['id']?>' data-courier='<?=$p['courier']?>'  data-address='<?=$p['address']?>'>Выбрать</a>" +
                                                "<br><strong>Адрес:</strong> <?=$p['address']?>" +
                                                "<br><strong>Время работы:</strong> <?=$p['work_schedule']?>" +
                                                "<br><strong>Телефон:</strong> <?=(count($p['phones']) >= 1) ? implode(",", $p['phones']) : ''?>" +
                                                '<br><strong>Доп. информация:</strong> <?=$p['trip_description']?>'





                                            }, {
                                                preset: "twirl#yellowStretchyIcon",
                                                // Отключаем кнопку закрытия балуна.
                                                balloonCloseButton: true,
                                                // Балун будем открывать и закрывать кликом по иконке метки.
                                                hideIconOnBalloonOpen: true
                                            });
                                            myMap.geoObjects.add(placemark_<?=$p['id']?>);
                                            <?php } ?>


                                        }
                                    </script>
                                    <div id="map-area" style="width: 100%; height: 400px;"></div>
                                    <?php
                                }
                                Pjax::end() ;?>

                            </div>

                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->


                <?php
                ActiveForm::end(); ?>


            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="container">


</div>
<?php $this->endBody() ?>
<script>(function(w, d, s, h, id) { w.roistatProjectId = id; w.roistatHost = h; var p = d.location.protocol == "https:" ? "https://" : "http://"; var u = /^.*roistat_visit=[^;]+(.*)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init"; var js = d.createElement(s); js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);})(window, document, 'script', 'cloud.roistat.com', '36dcfa0e19644e4e054d169e28e8455d');</script>
</body>

<!— Yandex.Metrika counter —> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter45332526 = new Ya.Metrika({ id:45332526, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/45332526" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!— /Yandex.Metrika counter —>


<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-102978351-1', 'auto');
    ga('send', 'pageview');

</script>

<script type="text/javascript">(window.Image ? (new Image()) : document.createElement('img')).src = 'https://vk.com/rtrg?p=VK-RTRG-140046-5Iov3';</script>

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type="text/javascript">
    (function (w, d) {
        w.amo_jivosite_id = 'iKQjooGCOu';
        var s = document.createElement('script'), f = d.getElementsByTagName('script')[0];
        s.id = 'amo_jivosite_js';
        s.type = 'text/javascript';
        s.async = true;
        s.src = 'https://forms.amocrm.ru/chats/jivosite/jivosite.js';
        f.parentNode.insertBefore(s, f);
    })(window, document);
</script>
<!-- {/literal} END JIVOSITE CODE -->
    
</html>
<?php $this->endPage() ?>
